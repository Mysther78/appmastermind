package com.mysther.MMBrassetDanoffre;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainActivity extends AppCompatActivity {
    private GameBoard gameBoard;
    private int nombreDeTourDeJeu;
    private int nombreDeChiffre;

    //TODO : MENU D'AIDE + SUR LE CLICK DU LOGO ON AFFICHE LA TETE DE LOUIS

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Connexion(View view) {


            if(view.getId() == findViewById(R.id.btn_facile).getId()) {
                nombreDeTourDeJeu = 10;
                nombreDeChiffre = 2;
            }
            else if(view.getId() == findViewById(R.id.btn_normal).getId()) {
                nombreDeTourDeJeu = 10;
                nombreDeChiffre = 3;
            }
            else if(view.getId() == findViewById(R.id.btn_difficile).getId()) {
                nombreDeTourDeJeu = 6;
                nombreDeChiffre = 3;
            }
            else if(view.getId() == findViewById(R.id.btn_perso).getId()) {
                nombreDeTourDeJeu = Integer.parseInt(((EditText)findViewById(R.id.editView_tourPerso)).getText().toString());
                nombreDeChiffre = Integer.parseInt(((EditText)findViewById(R.id.editView_colPerso)).getText().toString());
            }
            else {
                return;
            }

            setContentView(R.layout.game);

            TableLayout tableLayout = findViewById(R.id.gameBoard);
            gameBoard = new GameBoard(this, tableLayout, nombreDeTourDeJeu, nombreDeChiffre);
            gameBoard.init();
    }

    public void Deconnexion(View view) {
        setContentView(R.layout.activity_main);
    }

    public void validation(View view) {
        int tour = gameBoard.getTour();
        List<Integer> nombreRobot = gameBoard.getNombreRobot();
        List<Integer> nombreUtilisateur = new ArrayList<Integer>();
        TextView textViewErreur = findViewById(R.id.text_erreur_gameboard);
        View viewFucused = this.getCurrentFocus();

        //Verification des champs edittext
        for (int i = 0 ; i < nombreDeChiffre ; i++) {
            TextView editText = (EditText) findViewById(gameBoard.getId(tour, i+1));
            if(editText.getText().toString().equals("")) {
                textViewErreur.setText("L'un des champs de saisie est vide !");
                if (viewFucused != null) {//On vire le clavier
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(viewFucused.getWindowToken(), 0);
                }
                return; //On part de la methode
            }
            nombreUtilisateur.add(Integer.parseInt(editText.getText().toString())); //On les recupere
        }

        // Verification de non doublon dans le formulaire
        HashSet<Integer> setNombreUtilisateur = new HashSet<Integer>(nombreUtilisateur);
        if(setNombreUtilisateur.size() != nombreUtilisateur.size() || setNombreUtilisateur.size() != nombreDeChiffre){
            textViewErreur.setText("Tous les chiffres saisis doivent être differents !");
            if (viewFucused != null) { //On vire le clavier
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(viewFucused.getWindowToken(), 0);
            }
            return; //On part de la methode
        }

        textViewErreur.setText("");

        LinearLayout linearLayoutResGlobal = (LinearLayout) findViewById(gameBoard.getId(tour,nombreDeChiffre + 1));
        linearLayoutResGlobal.setVisibility(View.VISIBLE);
        LinearLayout linearLayoutResGood = (LinearLayout) linearLayoutResGlobal.getChildAt(0);
        TextView nbGood = (TextView) linearLayoutResGood.getChildAt(0);
        LinearLayout linearLayoutResBad = (LinearLayout) linearLayoutResGlobal.getChildAt(1);
        TextView nbBad = (TextView) linearLayoutResBad.getChildAt(0);
        //On les verifie par rapport au nombre robot
        if(nombreRobot.equals(nombreUtilisateur)) { //  REUSSI
            //Affichage du resultat
            nbGood.setText(String.valueOf(nombreDeChiffre));
            nbBad.setText("0");
            new AlertDialog.Builder(this)
                    .setTitle("Victoire !")
                    .setMessage("Vous avez gagné !!!\nLe nombre robot était bien " + nombreRobot.toString() + " !\nSouhaitez-vous rejouer ?")
                    .setPositiveButton(R.string.rejouer, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            TableLayout tableLayout = findViewById(R.id.gameBoard);
                            gameBoard.init();
                        }
                    })
                    .setNegativeButton("Quitter", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setContentView(R.layout.activity_main);
                        }
                    })
                    .setIcon(R.mipmap.ic_launcher_round)
                    .show();
        }
        else if(tour < nombreDeTourDeJeu) { // CONTINUE
            //Affichage du resultat
            int cptGood = 0;
            int cptBad = 0;
            Log.i("liste robot", String.valueOf(nombreRobot));
            Log.i("liste user", String.valueOf(nombreUtilisateur));
            for(int i = 0 ; i < nombreDeChiffre ; i++) {
                if (nombreRobot.get(i).equals(nombreUtilisateur.get(i)))
                    cptGood++;
                else if (nombreRobot.contains(nombreUtilisateur.get(i)))
                    cptBad++;
            }
            nbGood.setText(String.valueOf(cptGood));
            nbBad.setText(String.valueOf(cptBad));
            //On incremente le tour de jeu + On desactive les anciennes (en blanc) + on met les nouvelles cases à dispositions
            for (int i = 0 ; i < nombreDeChiffre ; i++) {
                TextView editText = (EditText) findViewById(gameBoard.getId(tour, i+1));
                editText.setInputType(InputType.TYPE_NULL);
            }
            tour++;
            gameBoard.setTour(tour);
            for (int i = 1 ; i < nombreDeChiffre + 1 ; i++) {
                TextView editText = (EditText) findViewById(gameBoard.getId(tour, i));
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                editText.setBackground(getDrawable(R.drawable.border));
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});

                //On active les prochains
            }

            // On met le focus sur la première colonne du prochain
            Handler mHandler= new Handler();
            mHandler.post(new Runnable() {
                        public void run() {
                            findViewById(gameBoard.getId(gameBoard.getTour(),1)).requestFocus();
                        }
                    });

        }
        else {
            new AlertDialog.Builder(this)
                    .setTitle("Défaite !")
                    .setMessage("Vous avez perdu...\nLe nombre robot était " + nombreRobot.toString() + " !\nSouhaitez-vous rejouer ?")
                    .setPositiveButton(R.string.rejouer, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            TableLayout tableLayout = findViewById(R.id.gameBoard);
                            gameBoard.init();
                        }
                    })
                    .setNegativeButton("Quitter", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setContentView(R.layout.activity_main);
                        }
                    })
                    .setIcon(R.mipmap.ic_launcher_round)
                    .show();
            int cptGood = 0;
            int cptBad = 0;
            Log.i("liste robot", String.valueOf(nombreRobot));
            Log.i("liste user", String.valueOf(nombreUtilisateur));
            for(int i = 0 ; i < nombreDeChiffre ; i++) {
                if (nombreRobot.get(i).equals(nombreUtilisateur.get(i)))
                    cptGood++;
                else if (nombreRobot.contains(nombreUtilisateur.get(i)))
                    cptBad++;
            }
            nbGood.setText(String.valueOf(cptGood));
            nbBad.setText(String.valueOf(cptBad));
        }
    }
}
