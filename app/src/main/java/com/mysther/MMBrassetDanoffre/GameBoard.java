package com.mysther.MMBrassetDanoffre;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameBoard {
    private MainActivity main;
    private TableLayout gameBoard;
    private int nombreLigne;
    private int nombreColonne;

    private int tour;
    private List<Integer> nombreRobot;


    public GameBoard(MainActivity mainActivity, TableLayout tableLayout, int nombreDeTourDeJeu, int nombreDeChiffre) {
        this.main = mainActivity;
        this.gameBoard = tableLayout;
        this.nombreLigne = nombreDeTourDeJeu + 1;
        this.nombreColonne = nombreDeChiffre + 2;
    }

    public int getId(int ligne, int colonne) {
        DecimalFormat formater = new DecimalFormat("00");
        String idString = formater.format(ligne)+ formater.format(colonne);
        return Integer.parseInt(idString);
    }

    public List<Integer> getNombreRobot() {
        return nombreRobot;
    }

    public int getTour() {
        return tour;
    }

    public void setTour(int tour) {
        this.tour = tour;
    }

    public void init() {

        nombreRobot = new ArrayList<Integer>();
        Random rand = new Random();
        int newRand;
        for(int i = 0 ; i < nombreColonne-2 ; i++) {
            do
                newRand = rand.nextInt(9);
            while (nombreRobot.contains(newRand));
            nombreRobot.add(newRand);
        }
        Log.i("Nombre robot", nombreRobot.toString());
        tour = 1;
        gameBoard.removeAllViews();

        for (int i = 0 ; i < nombreLigne ; i++) {
            TableRow tableRow = new TableRow(main);
            tableRow.setBackground(main.getDrawable(R.drawable.border)); //Met une bordure pour toute la ligne

            for (int j = 0; j < nombreColonne; j++) {
                if (j == (nombreColonne - 1) && i != 0) { // si derniere colonne
                    LinearLayout linearLayoutResGlobal = new LinearLayout(main); //Layout global du résultat
                    linearLayoutResGlobal.setId(getId(i,j));

                    LinearLayout linearLayoutResGood = new LinearLayout(main); //Layout good
                    TextView nbGood = new TextView(main); //Texte good
                    nbGood.setPadding(15,0,0,0);
                    linearLayoutResGood.addView(nbGood);

                    TextView rdGood = new TextView(main); //Rond good
                    rdGood.setText("\u2b24");
                    rdGood.setTextColor(Color.rgb(0,128,0));
                    rdGood.setPadding(10,0,0,0);
                    linearLayoutResGood.addView(rdGood);
                    linearLayoutResGlobal.addView(linearLayoutResGood);


                    LinearLayout linearLayoutResBad = new LinearLayout(main);
                    TextView nbBad = new TextView(main);
                    nbBad.setPadding(15,0,0,0);
                    linearLayoutResBad.addView(nbBad);

                    TextView rdBad = new TextView(main);
                    rdBad.setText("\u2b24");
                    rdBad.setTextColor(Color.rgb(255,165,0));
                    rdBad.setPadding(10,0,0,0);
                    linearLayoutResBad.addView(rdBad);
                    linearLayoutResGlobal.addView(linearLayoutResBad);

                    linearLayoutResGlobal.setOrientation(LinearLayout.VERTICAL);
                    linearLayoutResGlobal.setVisibility(View.INVISIBLE);
                    tableRow.addView(linearLayoutResGlobal);
                }
                else { // sinon
                    TextView tabledata = new TextView(main);
                    tabledata.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                    if (i == 0) {
                        //Première ligne de jeu !
                        //La decomposition en deux IF est importante pour que la première ligne tombe toujours dans cette partie là uniquement!
                        if (j != nombreColonne - 1 && j != 0) { //Si c'est pas la première, ni dernière colonne
                            tabledata.setText("?");
                            tabledata.setBackground(main.getDrawable(R.drawable.border));
                        }
                    } else if (j == 0) { //Si c'est la première colonne
                        //Colonne de gauche pour le numéro de ligne
                        tabledata.setText(String.valueOf(i));
                    } else if (j != nombreColonne - 1) {  //Si c'est pas la dernière colonne
                        //Données de jeu
                        //Il faut mettre des identifiants pour les retrouver apres
                        tabledata = new EditText(main);
                        tabledata.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        tabledata.setInputType(InputType.TYPE_NULL);
                        tabledata.setBackground(main.getDrawable(R.drawable.border_disabled)); //Met une bordure pour toute les cases au milieu de jeu
                        tabledata.setId(getId(i, j));
                        tabledata.setFilters(new InputFilter[]{new InputFilter.LengthFilter(0)}); //Protège de l'écriture de toutes les cases

                        tabledata.setOnKeyListener(new View.OnKeyListener() {
                            @Override
                            public boolean onKey(View v, int keyCode, KeyEvent event) {
                                TextView editText = (EditText) v;
                                if(event.getAction() == KeyEvent.ACTION_DOWN) {
                                    Log.i("My id", String.valueOf(v.getId()));
                                    Log.i("Action", String.valueOf(keyCode));
                                    if (keyCode == KeyEvent.KEYCODE_ENTER) {// Si le bouton ENTER est activé
                                        main.validation(v);
                                    }
                                    else { //On supprime la case
                                        editText.setText("");
                                    }

                                    //Set focus sur la prochaine case si on a pas supprimé avant
                                    if(keyCode != KeyEvent.KEYCODE_DEL) {
                                        try{
                                            int previousFocus = editText.getId();
                                            int nextFocus = previousFocus + 1;
                                            int endOfGameBoard = previousFocus + 2;

                                            //On verifie si on sort du jeu à +2 (si oui alors c'est la dernière case du gameboard)
                                            if(main.findViewById(endOfGameBoard) != null) {
                                                editText.clearFocus();
                                                main.findViewById(nextFocus).requestFocus();
                                            }
                                        } catch(Exception e) {Log.e("error", e.getStackTrace().toString());}
                                    }
                                }
                                return false;
                            }
                        });
                    }
                    tableRow.addView(tabledata);
                }
            }
            gameBoard.addView(tableRow);
        }

        //Mettre le tableau de jeu opérationnel
        for (int i = 1 ; i < nombreColonne - 1 ; i++) {
            TextView editText = (EditText) main.findViewById(getId(tour, i));
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            editText.setBackground(main.getDrawable(R.drawable.border));
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});
        }
    }

}
